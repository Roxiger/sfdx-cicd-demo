# GitLab CI/CD for Salesforce

Sample repo used in the webinar https://developer.salesforce.com/event/simplify-ci-cd and other live demos.

This Project is home to the [GitLab's version of the sample eBikes app](https://github.com/trailheadapps/ebikes-lwc) for Salesforce DX. It's a simple application that hosts an inventory of bicycles that can be customized.  

This GitLab project has been used for multiple demonstration purposes: 

* Webinar: [Video Recording](https://developer.salesforce.com/event/simplify-ci-cd)
* Dreamforce Presentation: [G Doc Link](https://docs.google.com/presentation/d/1_WSQ4IVAESsAa7nAV_KAm4Suf5miPpDAOUoIXUNuNb8/edit#slide=id.g5862141ecf_6_108)
* Trailhead DX Presentation: [Offline YouTube recording](https://youtu.be/tylPp9QlLu4) [G Doc Slide](https://docs.google.com/presentation/d/1dxD8xa9SFSGvHHD7RrpeXy2THbM30eStNEljeX34784/edit?usp=sharing)

# Example pipelines 

### Org Based Development

- **Pipeline 1**: Commit to [feature branch pipeline](https://gitlab.com/sfdx/sfdx-cicd-demo/pipelines/92927268) for scratch org review. 
- **Pipeline 2**: Merge to [Master branch, pipeline](https://gitlab.com/sfdx/sfdx-cicd-demo/pipelines/92928987) for pushing metadata to sandbox and production org. 

Here are the example Environment variables set to execute the pipelines shown above: 

**Variable** | **Value**
--- | ---
`DEPLOY_SCRATCH_ON_EVERY_COMMIT` | `true`
`DEVHUB_AUTH_URL` |`force://PlatformCLI::5Aep861j70kdjF1ZXM.....RETRACTED....LtDgJ0hF5MK@trailhead-test-automation-dev-ed.my.salesforce.com`
`PRODUCTION_AUTH_URL` |`force://PlatformCLI::5Aep861dR8ARTEERo7e5oXTq1LCEck8Ilv5f1ITys76XJc24c1r7sp2mhDznizylcMffOmkNv3Ntb.....RETRACTED....v29mDKuFo5@nosoftware-momentum-6427-dev-ed.cs40.my.salesforce.com/`
`SANDBOX_AUTH_URL` | `force://PlatformCLI::5Aep861ulCxXNoWflpm9.....RETRACTED...._ZKwhkYGo2cQG@inspiration-dream-8897-dev-ed.cs41.my.salesforce.com/`
`SALESFORCE_DEPLOYMENT_APPROACH` | `ORG_BASED`

> **Note:** To get the values for your auth URL's, make sure you're logged in with your `sfdx cli` locally and run the following command: `sfdx force:org:display --targetusername <username> --verbose`
 
### Package based Development

- **Pipteline 1**: Commit to feature branch pipeline for scratch org review is the same UX as linked above. 
- **Pipeline 2**: Merge to [Master branch, pipeline](https://gitlab.com/sfdx/sfdx-cicd-demo/pipelines/92931263) for pushing metadata to sandbox and production org.

Here are the example Environment variables set to execute the pipelines shown above: 

**Variable** | **Value**
--- | ---
`DEPLOY_SCRATCH_ON_EVERY_COMMIT` | `true`
`DEVHUB_AUTH_URL` | `force://PlatformCLI::5Aep861j70kdjF1ZXM.....RETRACTED....LtDgJ0hF5MK@trailhead-test-automation-dev-ed.my.salesforce.com`
`PRODUCTION_AUTH_URL` | `force://PlatformCLI::5Aep861dR8ARTEERo7e5oXTq1LCEck8Ilv5f1ITys76XJc24c1r7sp2mhDznizylcMffOmkNv3Ntb.....RETRACTED....v29mDKuFo5@nosoftware-momentum-6427-dev-ed.cs40.my.salesforce.com/`
`SANDBOX_AUTH_URL` | `force://PlatformCLI::5Aep861ulCxXNoWflpm9.....RETRACTED...._ZKwhkYGo2cQG@inspiration-dream-8897-dev-ed.cs41.my.salesforce.com/`


> **Note:** The variable `SALESFORCE_DEPLOYMENT_APPROACH` is intentionally omitted because by default the pipeline executes workflow for unlocked packaging. The `SALESFORCE_DEPLOYMENT_APPROACH` could explicitly be set to `PACKAGING` and it would result in the same pipeline.
